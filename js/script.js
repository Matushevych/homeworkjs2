function printPrimeNumbers(from, to) {
    for (from; from <= to; from++) {

        let notPrime = false;
        for (let i = 2; i <= from; i++) {
            if (from % i === 0 && i !== from) {
                notPrime = true;
            }
        }
        if (notPrime === false) {
            console.log(from);
        }
    }
}

let from = +prompt('Enter first number bigger than 1');
let to = +prompt('Enter second number bigger than first number');

while (Number.isInteger(from) === false && Number.isInteger(to) === false && from !== 0 && to !== 0 && from >= to) {
    alert("Error");
    let from = +prompt('Enter first number bigger than 1');
    let to = +prompt('Enter second number bigger than first number');
}

console.log(printPrimeNumbers(from, to));